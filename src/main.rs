use notify::{raw_watcher, RawEvent, RecursiveMode, Watcher};
use serde::Deserialize;
use std::collections::HashMap;
use std::env::set_current_dir;
use std::fs::File;
use std::io::{Error, ErrorKind, Read, Write};
use std::path::PathBuf;
use std::process::Command;
use std::sync::mpsc::channel;

#[derive(Debug, Deserialize)]
struct Language {
    depth:         usize,
    excluded_dirs: Vec<String>,
    extra_cmd:     String,
}

#[derive(Debug, Deserialize)]
struct Config {
    projects_dir:    String,
    gitignore_files: Vec<String>,
    languages:       HashMap<String, Language>,
}

impl Config {
    fn new() -> Result<Config, Error> {
        let f = File::open("config.json");
        return match f {
            Ok(mut file) => {
                let mut buffer = String::new();
                file.read_to_string(&mut buffer).unwrap();
                let config: Config = serde_json::from_str(buffer.as_ref()).expect("config was not well-formatted, some fields may be missing.");
                Ok(config)
            }
            Err(_) => Err(Error::new(ErrorKind::NotFound, "config file could not be found")),
        };
    }
}

#[derive(Debug)]
struct ProjectFolder<'a> {
    path:     &'a PathBuf,
    depth:    usize,
    chunks:   Vec<&'a str>,
    language: &'a Language,
}

impl<'a> ProjectFolder<'a> {
    fn new(path: &'a PathBuf, config: &'a Config) -> ProjectFolder<'a> {
        let chunks: Vec<&'a str> = path
            .strip_prefix(&config.projects_dir)
            .expect("the projects folder might not be properly configured in config.json")
            .to_str()
            .unwrap()
            .split(std::path::MAIN_SEPARATOR)
            .collect();
        let depth = chunks.len();
        let language: &Language = &config.languages[chunks[0]];

        ProjectFolder {
            path,
            depth,
            chunks,
            language,
        }
    }

    fn is_unix_hidden_dir(&self) -> bool {
        self.chunks[self.depth - 1].starts_with(".")
    }

    fn contains_excluded_dirs(&self) -> bool {
        if self.language.excluded_dirs.len() > 0 {
            for dir in self.language.excluded_dirs.iter() {
                if self.chunks.contains(&dir.as_str()) {
                    return true;
                }
            }
        }
        false
    }

    fn create_gitignore(&self, config: &Config) {
        let gitignore = self.path.join(".gitignore");
        if gitignore.exists() {
            let file = std::fs::OpenOptions::new().append(true).open(gitignore);
            self.extend_gitignore(file.unwrap(), &config).unwrap()
        } else {
            let file = std::fs::OpenOptions::new().create_new(true).append(true).open(gitignore);
            self.extend_gitignore(file.unwrap(), &config).unwrap()
        }
    }

    fn extend_gitignore(&self, mut gitignore_file: std::fs::File, config: &Config) -> Result<(), Error> {
        for file in config.gitignore_files.iter() {
            match gitignore_file.write((file.to_owned() + "\n").as_bytes()) {
                Ok(_) => continue,
                Err(_) => return Err(Error::new(ErrorKind::WriteZero, "couldn't write ".to_owned() + file + " to .gitignore")),
            };
        }
        Ok(())
    }

    fn process(&self, config: &Config) {
        if self.depth.eq(&self.language.depth) && !self.is_unix_hidden_dir() {
            if !self.contains_excluded_dirs() {
                assert!(set_current_dir(&self.path).is_ok());
                let number_files = std::fs::read_dir("./").unwrap().count();
                if number_files == 0 {
                    // only if creating a folder, and not moving it
                    if !self.language.extra_cmd.eq("") {
                        run_cmd(self.language.extra_cmd.as_str());
                        // some commands like 'cargo init' run 'git init' and create a .gitignore file
                    }
                    self.create_gitignore(&config);
                    // the .gitignore file is created in append mode,
                    // so that if the extra command creates a custom .gitignore file, those
                    // changes are not wiped
                    run_cmd("git init");
                    // if the extra command creates a git repo, it wouldn't be overwritten
                    // but even then, it's empty
                }
            }
        }
    }
}

fn main() {
    let config = Config::new().unwrap();
    let (tx, rx) = channel();
    let mut watcher = raw_watcher(tx).unwrap();
    watcher.watch(&config.projects_dir, RecursiveMode::Recursive).unwrap();

    loop {
        match rx.recv() {
            Ok(RawEvent {
                path: Some(abs_path),
                op: Ok(op),
                cookie: _,
            }) => match op {
                notify::op::CREATE => {
                    if abs_path.is_dir() {
                        let pf = ProjectFolder::new(&abs_path, &config);
                        pf.process(&config)
                    }
                }
                _ => continue,
            },
            Ok(event) => eprintln!("broken event: {:?}", event),
            Err(e) => eprintln!("watch error: {:?}", e),
        }
    }
}

fn run_cmd(cmd: &str) {
    let chunks: Vec<&str> = cmd.split(" ").collect();
    let (name, args) = chunks.split_at(1);
    let mut command = Command::new(name[0]);
    if let Ok(mut child) = command.args(args).spawn() {
        child.wait().expect("command wasn't running");
    }
}
