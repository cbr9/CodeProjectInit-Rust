#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT=$(basename "${DIR}");

cd "${DIR}" || return;
./target/release/"${PROJECT}" &
disown
