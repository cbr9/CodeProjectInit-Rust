### Motivation
If you're like me, you always forget to run "git init" whenever you create a project you deem as not important. But then it grows, or it turns up to be more important than you thought, and you wish you could run "git reset" because you fucked up.

This a project setup tool. Every time you create a folder in your projects folder, it will run "git init" and other related tools (like "go mod init" for Go or "cargo init" for Rust).
It can be customized via a config JSON file. Note that this assumes that your projects folder is structured by languages, and they're not all mixed together, as in
* /home/x/Code
   - Python
      * ...
   - Rust
      * ...
   - ...      
### Configuration instructions:
#### projects_dir
The absolute path to your projects root folder. E.g. /home/X/Code/
#### gitignore_files
A list of basic files you want to be appended to every .gitignore file
#### languages
Every language defined here represents a directory in $projects_dir.
Inside each of these languages, you can define up to three parameters:
- **depth**: the depth of the new folder, starting from but not including $projects_dir. E.g. in /home/x/Code/Python/test_project, depth would equal 2. This parameter allows to only run commands at a specific level in the tree structure, so that if, for example, you create a folder that will contain projects, that folder isn't converted into a project itself. Must have a specified value.
- **excluded_dirs**: a list defining already existing directory names in which no command will be run. Every Unix hidden folder (whose filename starts with a dot) is excluded. If there are no special folders, set it to an empty list.
- **extra_cmd**: an extra command, useful for some languages that have their own project setup tools, like Go and Rust with Go Modules or Cargo. Note that if these tools already set up a git repository, there is no problem in running "git init". From the official documentation: *Running git init in an existing repository is safe. It will not overwrite things that are already there*. Set to an empty string if no extra command is required.
